\entry {calling out}{43}{calling out}
\entry {calling in}{44}{calling in}
\entry {accepting calls}{44}{accepting calls}
\entry {mail}{45}{mail}
\entry {news}{45}{news}
\entry {spool directory}{46}{spool directory}
\entry {system spool directories}{46}{system spool directories}
\entry {.Status}{47}{.Status}
\entry {status files}{47}{status files}
\entry {.Xqtdir}{48}{.Xqtdir}
\entry {.Corrupt}{48}{.Corrupt}
\entry {.Failed}{49}{.Failed}
\entry {.Sequence}{49}{.Sequence}
\entry {.Temp}{49}{.Temp}
\entry {.Preserve}{49}{.Preserve}
\entry {.Received}{49}{.Received}
\entry {lock files in spool directory}{50}{lock files in spool directory}
\entry {LCK..sys}{50}{LCK..\var {sys}}
\entry {system lock files}{50}{system lock files}
\entry {LCK.XQT.NN}{50}{LCK.XQT.\var {NN}}
\entry {LXQ.cmd}{50}{LXQ.\var {cmd}}
\entry {L.xxx}{50}{L.\var {xxx}}
\entry {spool directory, cleaning}{51}{spool directory, cleaning}
\entry {cleaning the spool directory}{51}{cleaning the spool directory}
\entry {config file examples}{55}{config file examples}
\entry {changing spool directory}{55}{changing spool directory}
\entry {spool directory, changing}{55}{spool directory, changing}
\entry {anonymous UUCP}{55}{anonymous UUCP}
\entry {leaf site}{56}{leaf site}
\entry {sys file example (leaf)}{56}{sys file example (leaf)}
\entry {gateway}{58}{gateway}
\entry {sys file example (gateway)}{58}{sys file example (gateway)}
\entry {time strings}{61}{time strings}
\entry {chat scripts}{62}{chat scripts}
\entry {config file}{65}{config file}
\entry {main configuration file}{65}{main configuration file}
\entry {configuration file (config)}{65}{configuration file (config)}
\entry {UUCP system name}{66}{UUCP system name}
\entry {system name}{66}{system name}
\entry {spool directory, setting}{66}{spool directory, setting}
\entry {/usr/spool/uucp}{66}{/usr/spool/uucp}
\entry {public directory}{66}{public directory}
\entry {uucppublic}{66}{uucppublic}
\entry {/usr/spool/uucppublic}{66}{/usr/spool/uucppublic}
\entry {lock directory}{66}{lock directory}
\entry {unknown systems}{66}{unknown systems}
\entry {parity in login names}{66}{parity in login names}
\entry {configuration file (dialcode)}{68}{configuration file (dialcode)}
\entry {dialcode file}{68}{dialcode file}
\entry {dialcode configuration file}{68}{dialcode configuration file}
\entry {call out file}{68}{call out file}
\entry {call configuration file}{68}{call configuration file}
\entry {call out login name}{68}{call out login name}
\entry {call out password}{68}{call out password}
\entry {configuration file (call)}{68}{configuration file (call)}
\entry {passwd file}{69}{passwd file}
\entry {passwd configuration file}{69}{passwd configuration file}
\entry {configuration file (passwd)}{69}{configuration file (passwd)}
\entry {call in login name}{69}{call in login name}
\entry {call in password}{69}{call in password}
\entry {log file}{69}{log file}
\entry {statistics file}{70}{statistics file}
\entry {debugging file}{70}{debugging file}
\entry {sys file}{71}{sys file}
\entry {system configuration file}{71}{system configuration file}
\entry {configuration file (sys)}{71}{configuration file (sys)}
\entry {grades}{74}{grades}
\entry {port file}{89}{port file}
\entry {port configuration file}{89}{port configuration file}
\entry {configuration file (port)}{89}{configuration file (port)}
\entry {dial file}{93}{dial file}
\entry {dialer configuration file}{93}{dialer configuration file}
\entry {configuration file (dial)}{93}{configuration file (dial)}
\entry {grades implementation}{102}{grades implementation}
\entry {lock files}{104}{lock files}
\entry {execution file format}{105}{execution file format}
\entry {X.* file format}{105}{\file {X.*} file format}
\entry {UUCP protocol}{107}{UUCP protocol}
\entry {protocol, UUCP}{107}{protocol, UUCP}
\entry {initial handshake}{107}{initial handshake}
\entry {S UUCP protocol command}{111}{S UUCP protocol command}
\entry {UUCP protocol S command}{111}{UUCP protocol S command}
\entry {R UUCP protocol command}{114}{R UUCP protocol command}
\entry {UUCP protocol R command}{114}{UUCP protocol R command}
\entry {X UUCP protocol command}{115}{X UUCP protocol command}
\entry {UUCP protocol X command}{115}{UUCP protocol X command}
\entry {E UUCP protocol command}{116}{E UUCP protocol command}
\entry {UUCP protocol E command}{116}{UUCP protocol E command}
\entry {H UUCP protocol command}{117}{H UUCP protocol command}
\entry {UUCP protocol H command}{117}{UUCP protocol H command}
\entry {final handshake}{118}{final handshake}
\entry {g protocol}{118}{\samp {g} protocol}
\entry {protocol g}{118}{protocol \samp {g}}
\entry {f protocol}{123}{\samp {f} protocol}
\entry {protocol f}{123}{protocol \samp {f}}
\entry {t protocol}{124}{\samp {t} protocol}
\entry {protocol t}{124}{protocol \samp {t}}
\entry {e protocol}{125}{\samp {e} protocol}
\entry {protocol e}{125}{protocol \samp {e}}
\entry {G protocol}{125}{\samp {G} protocol}
\entry {protocol G}{125}{protocol \samp {G}}
\entry {i protocol}{125}{\samp {i} protocol}
\entry {protocol i}{125}{protocol \samp {i}}
\entry {j protocol}{129}{\samp {j} protocol}
\entry {protocol j}{129}{protocol \samp {j}}
\entry {x protocol}{131}{\samp {x} protocol}
\entry {protocol x}{131}{protocol \samp {x}}
\entry {y protocol}{131}{\samp {y} protocol}
\entry {protocol y}{131}{protocol \samp {y}}
\entry {d protocol}{134}{\samp {d} protocol}
\entry {protocol d}{134}{protocol \samp {d}}
\entry {h protocol}{134}{\samp {h} protocol}
\entry {protocol h}{134}{protocol \samp {h}}
\entry {v protocol}{134}{\samp {v} protocol}
\entry {protocol v}{134}{protocol \samp {v}}
